# Define builder image
FROM debian:9 as builder

## Set build args
ARG nginx_version=1.19.3
ARG LUAJIT_LIB=/usr/local/lib
ARG LUAJIT_INC=/usr/local/include/luajit-2.1

## Preinstall software
RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev

### Install jit
RUN wget -qO- https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20201229.tar.gz | tar -xz -C /tmp/ \
    && cd /tmp/luajit2-2.1-20201229 \
    && make \
    && make install

### Install libs
RUN wget -qO- https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.21.tar.gz | tar -xz -C /tmp/ \
    && cd /tmp/lua-resty-core-0.1.21 \
    && make install

RUN wget -qO- https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.10.tar.gz | tar -xz -C /tmp/ \
    && cd /tmp/lua-resty-lrucache-0.10 \
    && make install

### Install nginx devel kit and lua module
RUN wget -qO- https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz | tar -xz -C /tmp/ \
    && wget -qO- https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz | tar -xz -C /tmp/

## Get nginx sourcecode
RUN wget http://nginx.org/download/nginx-${nginx_version}.tar.gz && tar -xvzf nginx-${nginx_version}.tar.gz

# Build nginx
RUN cd nginx-${nginx_version} && ./configure \
--with-ld-opt="-Wl,-rpath,/usr/local/lib" \
--add-module=/tmp/ngx_devel_kit-0.3.1 \
--add-module=/tmp/lua-nginx-module-0.10.19 \
    && make && make install

# Build main image
FROM debian:9

# Set runtime env
ENV GREETINGS_FROM=use_1ess

## Prepare env

### Make dirs
RUN mkdir -p /usr/local/nginx/ && cd /usr/local/nginx/ && mkdir logs && mkdir sbin && mkdir conf && mkdir html

### Copy libs
COPY --from=builder /usr/local/lib/ /usr/local/lib/

### Copy htmls
COPY --from=builder /usr/local/nginx/html/50x.html /usr/local/nginx/html/index.html /usr/local/nginx/html/

### Copy additionals
COPY --from=builder /usr/local/nginx/conf/mime.types /usr/local/nginx/conf/

### Copy main binary
COPY --from=builder /usr/local/nginx/sbin/nginx /usr/local/nginx/sbin/

### Logs
RUN ln -s /dev/stdout /usr/local/nginx/logs/access.log 
RUN ln -s /dev/stderr /usr/local/nginx/logs/error.log 

## Run nginx
CMD ["/usr/local/nginx/sbin/nginx"]
