# raw-nginx

raw nginx Docker image

## Commands

Build command: 
> `docker build -t raw-nginx-jw:0.2 .`

Run command (assuming you are using external 8080 port and `my_default_nginx.conf` as configuration file name):
> `docker run -d --name raw-nginx-jw -p 8080:80 -v $(pwd)/my_default_nginx.conf:/usr/local/nginx/conf/nginx.conf raw-nginx-jw:0.2`

Interactive run command (for debug purpose):
> `docker run -it --rm --name raw-nginx-jw -v $(pwd)/my_default_nginx.conf:/usr/local/nginx/conf/nginx.conf raw-nginx-jw:0.2 /bin/bash`

Check lua module availability:
> `curl localhost:8080/hello`

Stop command:
> `docker stop raw-nginx-jw`

Remove container command:
> `docker rm raw-nginx-jw`
